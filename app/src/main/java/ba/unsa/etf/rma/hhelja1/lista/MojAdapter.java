package ba.unsa.etf.rma.hhelja1.lista;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by toshiba on 29.03.2017..
 */

public class MojAdapter extends ArrayAdapter<String>
{
    Context kontekst;
    int[] slike;
    String[] glumci;
    String[]godine;
    String[] mjesta;
    String[] rejtinzi;
    MojAdapter(Context c, String[] glumci, int[] sl, String[] god, String[] m, String[] rejt)
    {
        super(c, R.layout.list_item, R.id.textView2, glumci);
        this.kontekst = c;
        slike = sl;
        this.glumci = glumci;
        mjesta = m;
        godine = god;
        rejtinzi = rejt;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) kontekst.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View red = inflater.inflate(R.layout.list_item, parent, false);
        ImageView mojaSLika = (ImageView) red.findViewById(R.id.imageView3);
        TextView tekstGlumci = (TextView) red.findViewById(R.id.textView2);
        TextView tekstGodine = (TextView) red.findViewById(R.id.textView3);
        TextView tekstMjesto = (TextView) red.findViewById(R.id.textView);
        TextView tekstRejting = (TextView) red.findViewById(R.id.textView4);

        mojaSLika.setImageResource(slike[position]);
        tekstGlumci.setText(glumci[position]);
        tekstGodine.setText(godine[position]);
        tekstMjesto.setText(mjesta[position]);
        tekstRejting.setText(rejtinzi[position]);
        return red;
    }
}
