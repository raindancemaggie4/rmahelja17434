package ba.unsa.etf.rma.hhelja1.lista;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;


public class aktivnost extends AppCompatActivity {


    ListView lista;
    String[] glumci;
    String[] rejting;
    String[] mjestoRodjenja;
    String[] godinaRodjenja;
    int[] slike = {R.drawable.glumac1, R.drawable.glumac2, R.drawable.glumac3, R.drawable.glumac4, R.drawable.glumac5, R.drawable.glumac6, R.drawable.glumac7, R.drawable.glumac8};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aktivnost);


        Resources res = getResources();
        glumci = res.getStringArray(R.array.Glumci);
        godinaRodjenja = res.getStringArray(R.array.Godine);
        mjestoRodjenja = res.getStringArray(R.array.Mjesto);
        rejting = res.getStringArray(R.array.Rejting);

        lista = (ListView) findViewById(R.id.list);

        MojAdapter adapter = new MojAdapter(this, glumci, slike, godinaRodjenja, mjestoRodjenja, rejting);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(aktivnost.this, BiografijaAktivnost.class);
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });

    }

    //Klik na dugme resziseri
    public void PrebaciNaReziseriAktivnost(View view) {
        Intent Intent = new Intent(aktivnost.this, reziseriAktivnost.class);
        aktivnost.this.startActivity(Intent);
    }

    //Klik na dugme zanrovi
    public void PrebaciNaZanroviAktivnost(View view)
    {
        Intent intent = new Intent(this, zanroviAktivnost.class);
        startActivity(intent);
    }




}
