package ba.unsa.etf.rma.hhelja1.lista;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.List;

public class zanroviAktivnost extends AppCompatActivity {

    int[] slikeZanrova = {R.drawable.zanrovi1,R.drawable.zanrovi2,R.drawable.zanrovi3,
            R.drawable.zanrovi4,R.drawable.zanrovi5,R.drawable.zanrovi6,R.drawable.zanrovi7,R.drawable.zanrovi8,R.drawable.zanrovi9};
    String[] textZanrova;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zanrovi_aktivnost);

        Resources res = getResources();
        textZanrova = res.getStringArray(R.array.Zanrovi);

        ListView listaZanrova = (ListView) findViewById(R.id.listaZanrovi);

        MojAdapterZaZanrove adapter = new MojAdapterZaZanrove(this,textZanrova,slikeZanrova);
        listaZanrova.setAdapter(adapter);
    }

    //Klik na dugme reziseri
    public void PrebaciNaReziseriAktivnost(View view)
    {
        Intent intent = new Intent(this, reziseriAktivnost.class);
        startActivity(intent);

    }

    //Klik na dugme glumci
    public void PrebaciNaGlumciAktivnost(View view)
    {
        Intent intent = new Intent(this, aktivnost.class);
        startActivity(intent);
    }
}
