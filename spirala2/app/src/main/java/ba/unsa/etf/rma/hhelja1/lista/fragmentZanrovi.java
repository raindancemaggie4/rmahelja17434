package ba.unsa.etf.rma.hhelja1.lista;

import android.app.Fragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Created by toshiba on 13.04.2017..
 */

public class fragmentZanrovi extends Fragment {
    int[] slikeZanrova = {R.drawable.zanrovi1,R.drawable.zanrovi2,R.drawable.zanrovi3,
            R.drawable.zanrovi4,R.drawable.zanrovi5,R.drawable.zanrovi6,R.drawable.zanrovi7,R.drawable.zanrovi8,R.drawable.zanrovi9};
    String[] textZanrova;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_zanrovi,container,false);


        Resources res = getResources();
        textZanrova = res.getStringArray(R.array.Zanrovi);

        ListView listaZanrova = (ListView) view.findViewById(R.id.listaZanrovi);

        MojAdapterZaZanrove adapter;
        adapter = new MojAdapterZaZanrove(getActivity(),textZanrova,slikeZanrova);
        listaZanrova.setAdapter(adapter);

        return view;
    }
}
