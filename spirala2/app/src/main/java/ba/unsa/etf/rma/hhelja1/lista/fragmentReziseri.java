package ba.unsa.etf.rma.hhelja1.lista;

import android.app.Fragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by toshiba on 13.04.2017..
 */

public class fragmentReziseri extends Fragment {
    ListView listaRezisera;
    String[] reziseri;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_reziseri,container,false);
        Resources res = getResources();

        reziseri = res.getStringArray(R.array.Reziseri);
        listaRezisera = (ListView) view.findViewById(R.id.listaRezisera);
        final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, reziseri);
        listaRezisera.setAdapter(adapter);

        return view;
    }
}
