package ba.unsa.etf.rma.hhelja1.lista;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class reziseriAktivnost extends AppCompatActivity {

    ListView listaRezisera;
    String[] reziseri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reziseri_aktivnost);


        Resources res = getResources();

        reziseri = res.getStringArray(R.array.Reziseri);
        listaRezisera = (ListView) findViewById(R.id.listaRezisera);
        final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, reziseri);
        listaRezisera.setAdapter(adapter);
    }

    //Klik na dugme glumci
    public void PrebaciNaGlumciAktivnost(View v)
    {
        Intent intent = new Intent(reziseriAktivnost.this, aktivnost.class);
        startActivity(intent);
    }

    //Klik na dugme zanrovi
    public void PrebaciNaZanroviAktivnost(View view)
    {
        Intent intent = new Intent(this, zanroviAktivnost.class);
        startActivity(intent);
    }
}
