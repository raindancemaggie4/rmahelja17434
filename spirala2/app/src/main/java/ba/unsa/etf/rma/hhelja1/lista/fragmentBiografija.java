package ba.unsa.etf.rma.hhelja1.lista;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by toshiba on 13.04.2017..
 */

public class fragmentBiografija extends Fragment {
    View someView;
    View root;
    TextView spol;
    String[] moguciSpolovi;
    TextView ime;
    TextView rodjenje;
    TextView smrt;
    TextView biografija;
    TextView mjesto;
    TextView link;
    ImageView slika;

    RelativeLayout lejaut;

    String[] imena;
    String[] rodjenja;
    String[] biografije;
    String[] linkovi;
    String[] mjesta;
    String[] spolovi;
    int[] slike = {R.drawable.glumac1, R.drawable.glumac2, R.drawable.glumac3, R.drawable.glumac4, R.drawable.glumac5, R.drawable.glumac6, R.drawable.glumac7, R.drawable.glumac8};
    int position;
    KlikIzBiografije interfejs;
    public interface KlikIzBiografije
    {
        public void OtvoriLink(int position);
        public void Podijeli(int position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.interfejs = (KlikIzBiografije) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();

        if(bundle!=null)
        {
            position = bundle.getInt("pozicija");
        }
        View view = inflater.inflate(R.layout.fragment_biografija, container, false);

        Resources res = getResources();

        imena = res.getStringArray(R.array.Glumci);
        rodjenja = res.getStringArray(R.array.Godine);
        spolovi = res.getStringArray(R.array.Spolovi);
        linkovi = res.getStringArray(R.array.Linkovi);
        mjesta = res.getStringArray(R.array.Mjesto);
        biografije = res.getStringArray(R.array.Biografije);
        moguciSpolovi = res.getStringArray(R.array.moguciSpolovi);

        //nadji sve komponente gui-a
        ime = (TextView) view.findViewById(R.id.textIme);
        rodjenje = (TextView) view.findViewById(R.id.textRodjenje);

        biografija = (TextView) view.findViewById(R.id.textBio);
        link = (TextView) view.findViewById(R.id.textLink);
        spol = (TextView) view.findViewById(R.id.textSpol);
        mjesto = (TextView) view.findViewById(R.id.textMjesto);
        slika = (ImageView) view.findViewById(R.id.slikaGlumca);

        ime.setText(imena[position]);
        rodjenje.setText(rodjenja[position]);
        link.setText(linkovi[position]);
        mjesto.setText(mjesta[position]);
        spol.setText(spolovi[position]);
        slika.setImageResource(slike[position]);
        biografija.setText(biografije[position]);

        lejaut = (RelativeLayout) view.findViewById(R.id.constrLejaut);


        if(spolovi[position].equals(moguciSpolovi[1])) {
            lejaut.setBackgroundColor(res.getColor(R.color.mat));
        }

        Button podijeli = (Button) view.findViewById(R.id.dugmePodijeli);
        podijeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfejs.Podijeli(position);
            }
        });
        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfejs.OtvoriLink(position);
            }
        });
return view;

    }


/*
    public void OtvoriLink(View view)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkovi[position]));
        startActivity(browserIntent);
    }

    //dugme podijeli
    public void Podijeli(View view)
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, biografije[position]);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);

    }

*/

}
