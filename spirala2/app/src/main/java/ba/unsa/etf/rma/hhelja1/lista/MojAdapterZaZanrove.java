package ba.unsa.etf.rma.hhelja1.lista;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by toshiba on 30.03.2017..
 */

public class MojAdapterZaZanrove extends ArrayAdapter<String> {

    Context kontekst;
    int[] slike;
    String[] zanrovi;
    MojAdapterZaZanrove(Context c, String[] zanr, int[] sl)
    {

        super(c,R.layout.zanrovi_list_item,zanr);
        slike = sl;
        zanrovi = zanr;
        kontekst = c;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) kontekst.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View red = inflater.inflate(R.layout.zanrovi_list_item,parent, false);

        ImageView zanroviSlika = (ImageView) red.findViewById(R.id.slikaZanr);
        TextView zanroviText = (TextView) red.findViewById(R.id.textZanr);

        zanroviSlika.setImageResource(slike[position]);
        zanroviText.setText(zanrovi[position]);

        return red;
    }
}
