package ba.unsa.etf.rma.hhelja1.lista;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class BiografijaAktivnost extends AppCompatActivity {
    View someView;
    View root;
    TextView spol;
    String[] moguciSpolovi;
    TextView ime;
    TextView rodjenje;
    TextView smrt;
    TextView biografija;
    TextView mjesto;
    TextView link;
    ImageView slika;

    ConstraintLayout lejaut;

    String[]imena;
    String[]rodjenja;
    String[]biografije;
    String[]linkovi;
    String[]mjesta;
    String[] spolovi;
    int[] slike = {R.drawable.glumac1, R.drawable.glumac2, R.drawable.glumac3, R.drawable.glumac4, R.drawable.glumac5, R.drawable.glumac6, R.drawable.glumac7, R.drawable.glumac8};
    int position;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biografija_aktivnost);

        Intent intent = getIntent();
        position = intent.getExtras().getInt("position");

        Resources res = getResources();

        imena = res.getStringArray(R.array.Glumci);
        rodjenja = res.getStringArray(R.array.Godine);
        spolovi = res.getStringArray(R.array.Spolovi);
        linkovi = res.getStringArray(R.array.Linkovi);
        mjesta = res.getStringArray(R.array.Mjesto);
        biografije = res.getStringArray(R.array.Biografije);
        moguciSpolovi = res.getStringArray(R.array.moguciSpolovi);

       //nadji sve komponente gui-a
        ime = (TextView) findViewById(R.id.textIme);
        rodjenje = (TextView) findViewById(R.id.textRodjenje);
        smrt = (TextView) findViewById(R.id.textSmrt);
        biografija = (TextView) findViewById(R.id.textBio);
        link = (TextView) findViewById(R.id.textLink);
        spol = (TextView) findViewById(R.id.textSpol);
        mjesto = (TextView) findViewById(R.id.textMjesto);
        slika = (ImageView) findViewById(R.id.slikaGlumca);

        ime.setText(imena[position]);
        rodjenje.setText(rodjenja[position]);
        link.setText(linkovi[position]);
        mjesto.setText(mjesta[position]);
        spol.setText(spolovi[position]);
        slika.setImageResource(slike[position]);
        biografija.setText(biografije[position]);


        lejaut = (ConstraintLayout) findViewById(R.id.constrLejaut);


      /* if(spolovi[position].equals(moguciSpolovi[1])) {
            lejaut.setBackgroundColor(R.color.mat);
        }*/

    }
    public void OtvoriLink(View view)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkovi[position]));
        startActivity(browserIntent);
    }

    //dugme podijeli
    public void Podijeli(View view)
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, biografije[position]);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);

    }
}
