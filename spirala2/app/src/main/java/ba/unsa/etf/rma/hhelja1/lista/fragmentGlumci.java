package ba.unsa.etf.rma.hhelja1.lista;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by toshiba on 13.04.2017..
 */

public class fragmentGlumci extends Fragment {
    ListView lista;
    String[] glumci;
    String[] rejting;
    String[] mjestoRodjenja;
    String[] godinaRodjenja;
    int[] slike = {R.drawable.glumac1, R.drawable.glumac2, R.drawable.glumac3, R.drawable.glumac4, R.drawable.glumac5, R.drawable.glumac6, R.drawable.glumac7, R.drawable.glumac8};

    KlikNaListu interfejs;

    public interface KlikNaListu
    {
        public void KlikNaGlumca(int position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.interfejs = (KlikNaListu) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_glumci,container,false);
        Resources res = getResources();
        glumci = res.getStringArray(R.array.Glumci);
        godinaRodjenja = res.getStringArray(R.array.Godine);
        mjestoRodjenja = res.getStringArray(R.array.Mjesto);
        rejting = res.getStringArray(R.array.Rejting);

        lista = (ListView) view.findViewById(R.id.list);

        MojAdapter adapter = new MojAdapter(getActivity(), glumci, slike, godinaRodjenja, mjestoRodjenja, rejting);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                interfejs.KlikNaGlumca(position);
            }
        });

        return view;
    }
}
