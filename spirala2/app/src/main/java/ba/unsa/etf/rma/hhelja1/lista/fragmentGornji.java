package ba.unsa.etf.rma.hhelja1.lista;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

/**
 * Created by toshiba on 13.04.2017..
 */

public class fragmentGornji extends Fragment  {
    Button dugmeGlumci;
    Button dugmeReziseri;
    Button dugmeZanrovi;
    ImageButton dugmeJezik;
    Klik interfejs;

    public interface Klik{
        public void KlikNaG();
        public void KlikNaR();
        public void KlikNaZ();
        public void KlikNaJ();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.interfejs = (Klik) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_gornji, container, false);
        dugmeGlumci = (Button) view.findViewById(R.id.dugmeGlumci);
       dugmeGlumci.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               interfejs.KlikNaG();
           }
       });

        dugmeReziseri = (Button) view.findViewById(R.id.dugmeReziseri);
        dugmeReziseri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfejs.KlikNaR();
            }
        });

        dugmeZanrovi = (Button) view.findViewById(R.id.dugmeZanrovi);
        dugmeZanrovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfejs.KlikNaZ();
            }
        });

        dugmeJezik = (ImageButton) view.findViewById(R.id.dugmeJezik);
        dugmeJezik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfejs.KlikNaJ();
            }
        });

        String trenutniJezik = getResources().getString(R.string.locale);
        if(trenutniJezik.equals("en")) {
            dugmeJezik.setImageResource(R.drawable.bos);
        }
        else
            dugmeJezik.setImageResource(R.drawable.eng);



        return view;
    }

    /*@Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.dugmeGlumci:
                    KlikInterfejs.KlikNaGlumce();
                    break;
            }
        }
        catch(ClassCastException ex)
        {

        }
    }*/

    /*




    public interface Klik
    {
        public void KlikNaGlumce();
    }*/


}

