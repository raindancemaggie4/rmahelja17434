package ba.unsa.etf.rma.hhelja1.lista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.icu.text.DateFormat;
import android.net.Uri;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.Locale;

import ba.unsa.etf.rma.hhelja1.lista.fragmentGlumci.KlikNaListu;
import ba.unsa.etf.rma.hhelja1.lista.fragmentGornji.Klik;

public class glavna extends AppCompatActivity implements Klik, KlikNaListu, fragmentBiografija.KlikIzBiografije {

    String[]biografije;
    String[]linkovi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_glavna);
        Resources res = getResources();
        linkovi = res.getStringArray(R.array.Linkovi);
        biografije = res.getStringArray(R.array.Biografije);

        Boolean siril = false;

        android.app.FragmentManager menadzer = getFragmentManager();
        FrameLayout frameGornji = (FrameLayout) findViewById(R.id.frameGornji);
        FrameLayout frameSiriLijevi = (FrameLayout) findViewById(R.id.frameLijevi);
        //znaci ekran je siri
        if(frameSiriLijevi!=null)
        {

        }
        else{
        fragmentGornji fragmentDugmad = (fragmentGornji) menadzer.findFragmentById(R.id.frameGornji);

            if (fragmentDugmad == null) {
                fragmentDugmad = new fragmentGornji();
                menadzer.beginTransaction().replace(R.id.frameGornji, fragmentDugmad).commit();
            }

            }
    }

    @Override
    public void KlikNaG() {

        android.app.FragmentManager menadzer = getFragmentManager();

        FrameLayout frameDonji = (FrameLayout) findViewById(R.id.frameDonji);

        fragmentGlumci  f1 = new fragmentGlumci();
        menadzer.beginTransaction().replace(R.id.frameDonji,f1).addToBackStack("glum").commit();

    }

    @Override
    public void KlikNaR() {
        android.app.FragmentManager fg = getFragmentManager();

        FrameLayout frameDonji = (FrameLayout) findViewById(R.id.frameDonji);

        fragmentReziseri f = new fragmentReziseri();
            fg.beginTransaction().replace(R.id.frameDonji, f).addToBackStack("rez").commit();

        }

    @Override
    public void KlikNaZ() {
        android.app.FragmentManager fg = getFragmentManager();

        FrameLayout frameDonji = (FrameLayout) findViewById(R.id.frameDonji);

        fragmentZanrovi f = new fragmentZanrovi();
        fg.beginTransaction().replace(R.id.frameDonji, f).addToBackStack("zanr").commit();

    }

    @Override
    public void KlikNaJ() {
        String trenutni =  getResources().getString(R.string.locale);


        String eng  = "en";
        String bos = "bs";
        Locale locale;
        if(trenutni.equals("en"))
            locale = new Locale(bos);

        else
            locale = new Locale(eng);

        Locale.setDefault(locale);

            Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config,getResources().getDisplayMetrics());



        Intent intent = new Intent(glavna.this, glavna.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void KlikNaGlumca(int position) {

        android.app.FragmentManager fg = getFragmentManager();
        FrameLayout frameSiriLijevi = (FrameLayout) findViewById(R.id.frameLijevi);
        if(frameSiriLijevi==null) {
            FrameLayout frameDonji = (FrameLayout) findViewById(R.id.frameDonji);

            Bundle bundle = new Bundle();
            bundle.putInt("pozicija", position);
            fragmentBiografija f = new fragmentBiografija();
            f.setArguments(bundle);
            fg.beginTransaction().replace(R.id.frameDonji, f).addToBackStack("bio").commit();
        }
        else
        {
            FrameLayout frameDesni = (FrameLayout) findViewById(R.id.frameDesni);
            Bundle bundle = new Bundle();
            bundle.putInt("pozicija", position);
            fragmentBiografija f = new fragmentBiografija();
            f.setArguments(bundle);
            fg.beginTransaction().replace(R.id.frameDesni, f).addToBackStack("bio").commit();
        }


    }
    @Override
    public void onBackPressed() {

        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();

        } else {
            getFragmentManager().popBackStack();
        }

    }

    public void KlikOstalo(View view)
    {
        android.app.FragmentManager menadzer = getFragmentManager();
        fragmentReziseri f = new fragmentReziseri();
        menadzer.beginTransaction().replace(R.id.frameLijevi,f).commit();

        fragmentZanrovi fz = new fragmentZanrovi();
        menadzer.beginTransaction().replace(R.id.frameDesni,fz).commit();

    }

    public void KlikGlumci(View view)
    {

        android.app.FragmentManager menadzer = getFragmentManager();
        fragmentGlumci f = new fragmentGlumci();
        menadzer.beginTransaction().replace(R.id.frameLijevi,f).commit();
        FrameLayout frejm = (FrameLayout) findViewById(R.id.frameDesni);
        frejm.removeAllViews();

    }



    @Override
    public void OtvoriLink(int position) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkovi[position]));
        startActivity(browserIntent);

    }

    @Override
    public void Podijeli(int position) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, biografije[position]);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);

    }
}

